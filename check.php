<?php
session_start();
class Users
{
/**
* User constructor.
* @param $name
* @param $surname
* @param $role
* @param $login
* @param $password
*/
public function __construct($name, $surname,$role,$lang)
{
$this->name = $name;
$this->surname = $surname;
$this->role = $role;
$this->lang=$lang;
}
public function getLang(){
    return $this->lang;
}
public function isAdmin(){
    return $this->role=='admin';
}
    public function isManager(){
        return $this->role=='manager';
    }
    public function isClient(){
        return $this->role=='client';
}
}

class admin extends Users
{
public function gotoNext()
{
header("Location: admin.php");
}
}

class client extends Users
{
public function gotoNext()
{
header("Location: client.php");
}
}

class manager extends Users
{
public function gotoNext()
{
header("Location: manager.php");
}
}
$arr = [
    ['name'=>'Василий','surname'=>'Лоханкин','login' => 'Vasisualiy', 'password' => '12345', 'lang' => 'ru','role'=>'admin'],
    ['name'=>'Афанасий','surname'=>'Цукерберг', 'login' => 'Afanasiy', 'password' => '54321', 'lang' => 'en','role'=>'client'],
    ['login' => 'Petro', 'name'=>'Петр','surname'=>'Инкогнито','password' => 'EkUC42nzmu', 'lang' => 'ua','role'=>'manager'],
    ['login' => 'Pedrolus', 'name'=>'Педро','surname'=>'Миланов', 'password' => 'Cogito_ergo_sum', 'lang' => 'it','role'=>'client'],
    ['login' =>'Sasha','name'=>'Александр','surname'=>'Александров',  'password' => 'Ignorantia_non_excusat', 'role'=>'manager' ]
];
$roles= [
    'admin'=>admin::class,
    'manager'=>manager::class,
    'client'=>client::class,
];
$translate = [
    'ru' => ['Hello' => 'Здравствуйте', 'You' => 'Вы', 'can' => 'можете', 'change' => 'изменить', 'language' => 'язык', 'below' => 'ниже', 'Personal cabinet' => 'Личный кабинет', 'Control panel' => 'Панель управления', 'Name' => 'Имя', 'Surname' => 'Фамилия', 'Role' => 'Роль', 'You can change your role below :' => 'Вы можете сменить вашу роль на сайте ниже :'],
    'ua' => ['Hello' => 'Здравствуйте', 'You' => 'Ви', 'can' => 'можете', 'change' => 'змінити', 'language' => 'мову', 'below' => 'нижче', 'Personal cabinet' => 'Особистий кабінет', 'Control panel' => 'Панель управління', 'Name' => 'Ім`я', 'Surname' => 'Прізвище', 'Role' => 'Роль', 'You can change your role below :' => 'Ви можете змінити вашу роль на сайте нижче :'],
    'en' => ['Hello' => 'Hello', 'You' => 'You', 'can' => 'can', 'change' => 'change', 'language' => 'language', 'below' => 'below', 'Personal cabinet' => 'Personal account', 'Control panel' => 'Control panel', 'Name' => 'Name', 'Surname' => 'Surname', 'Role' => 'Role', 'You can change your role below :' => 'You can change your role below :'],
    'it' => ['Hello' => 'Salve', 'You' => 'Puoi', 'can' => 'cambiare', 'change' => 'qui', 'language' => 'la lingua', 'below' => 'sotto', 'Personal cabinet' => 'Ufficio privato', 'Control panel' => 'Pannello di comando', 'Name' => 'Nome', 'Surname' => 'Cognome', 'Role' => 'Ruolo', 'You can change your role below :' => 'Puoi cambiare il tuo ruolo qui sotto :'],
];

function auth($arr)
{
    if (!isset($_SESSION['id'])) {
        return null;
    }
    $id=$_SESSION['id'];
    $user=$arr[$id];
    if($user['role']=='admin'){
        return $user = new admin($user['name'],$user['surname'],$user['role'], $_SESSION['lang']);
    }
    if($user['role']=='manager'){
            return $user = new manager($user['name'],$user['surname'],$user['role'], $_SESSION['lang']);
        }
    if($user['role']=='client'){
            return $user = new client($user['name'],$user['surname'],$user['role'], $_SESSION['lang']);
        }
}
$user = auth($arr);
?>