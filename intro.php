<?
if ($_POST['lang']) {
$_SESSION['lang'] = $_POST['lang'];
}
if ($user->getLang()) {
require_once("language.php");
echo $translate[$lang]['Hello'] . ' ' . $user->name . ' ' . $user->surname . '. ' . $translate[$lang]['You'] . ' ' . $translate[$lang]['can'] . ' ' . $translate[$lang]['change'] . ' ' . $translate[$lang]['language'] . ' ' . $translate[$lang]['below'];
}
if (empty($user->getLang())) {
echo 'Change language to continue';
}
?>

<form method="POST">
    <select name="lang">
        <option value="ru">ru</option>
        <option value="ua">ua</option>
        <option value="it">it</option>
        <option value="en">en</option>
    </select>
    <input type="submit"/>
</form>