<?php
require_once("check.php");
if($user->isClient()){
    header('HTTP/1.0 403 Forbidden');
}
else {
    require_once("language.php");
    $temp = $user->role;
    if ($_POST['profile']) {
        $user->role = $_POST['profile'];
    }
    if ($temp != $user->role && $_POST['profile']) {
        header("Location: index.php");
    }
    echo $translate[$lang]['You can change your role below :'];
        ?>
        <form method="POST">
            <select name="profile">
                <option value="admin">admin</option>
                <option value="manager">manager</option>
                <option value="client">client</option>
            </select>
            <input type="submit"/>
        </form>
        <?
}session_destroy();
?>